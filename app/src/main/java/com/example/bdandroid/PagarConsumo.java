package com.example.bdandroid;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PagarConsumo extends AppCompatActivity {
    private EditText userN;
    private TextView tPago;
    private Conexion c;
    private int lecturaAnterior;
    private int lecturaActual;
    private double total;
    private ListView lv;
    private boolean consulta = false;
    private  double totalD;
    private boolean pagoAceptado = false;
    private String usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagar_consumo);
        c = new Conexion(this,"DBconsumidores",null,1);
        Bundle param = this.getIntent().getExtras();
        if(param!=null){
            usuario = param.getString("usuario");
            userN = findViewById(R.id.nUserConsumo);
            userN.setText(usuario);
        }
    }

    public void consultarConsumo(View v){
        userN = findViewById(R.id.nUserConsumo);
        if(!userN.getText().toString().isEmpty()){
            SQLiteDatabase op = c.getReadableDatabase();
            usuario = userN.getText().toString();
            Cursor cursor = op.rawQuery("SELECT * FROM consumidores WHERE nusuario = '"+usuario+"'",null);

            lv = findViewById(R.id.lv1);
            lv.setAdapter(null);
            ArrayList<String>  r = new ArrayList<>();

            if(cursor.moveToFirst()) {
                do {
                    r.add(cursor.getString(0) + " - " + cursor.getString(1) + " - " + cursor.getString(2) + " - " + cursor.getString(3) + " - " + cursor.getString(4));
                }while(cursor.moveToNext());
                cursor.close();
            }

            if(r.isEmpty()){
                consulta=false;
                mostrarMensaje("El número de usuario que ingresaste ¡NO EXISTE!, o no cuenta con adeudos, verifica...");
                userN.setText("");
                op.close();
            }else {
                consulta=true;
                mostrarMensaje("Usuario ¡ENCONTRADO!, tiene adeudos...");
                ArrayAdapter<String> adapt = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, r);
                lv.setAdapter(adapt);
                op.close();
            }
        }
    }

    public void aceptarPago(View v){
        userN = findViewById(R.id.nUserConsumo);
        if(consulta){
            SQLiteDatabase op = c.getReadableDatabase();
            String usuario = userN.getText().toString();
            Cursor cursor = op.rawQuery("SELECT * FROM consumidores WHERE nusuario = '"+usuario+"'",null);
            totalD=0.0;

            if(cursor.moveToFirst()) {
                do {
                    totalD+=cursor.getDouble(4);
                }while(cursor.moveToNext());
                cursor.close();
            }
            pagoAceptado = true;
            tPago = findViewById(R.id.total);
            String totalDS = String.format("%.2f",totalD);
            tPago.setText("Total a pagar: " + totalDS);

        }else{
            pagoAceptado = false;
            mostrarMensaje("¡ERROR! verifica tus datos...");
        }
    }

    public void pagarConsumo(View v){
        userN = findViewById(R.id.nUserConsumo);
        if(consulta && pagoAceptado){
            SQLiteDatabase op = c.getReadableDatabase();
            String usuario = userN.getText().toString();
            //Cursor cursor = op.rawQuery("DELETE * FROM consumidores WHERE nusuario = '"+usuario+"'",null);
            op.execSQL("DELETE FROM consumidores WHERE nusuario = '"+usuario+"'");
            /*if(cursor.moveToFirst()) {
                do {

                }while(cursor.moveToNext());
                cursor.close();
            }*/
            mostrarMensaje("Consumo PAGADO, eliminando registros...");
            op.close();
        }else{
            mostrarMensaje("¡ERROR! verifica tus datos...");
        }
    }

    public void mostrarMensaje(String m){
        Toast msj = Toast.makeText(getApplicationContext(),m,Toast.LENGTH_LONG);
        msj.show();
    }

    public void regresar(View v){
        finish();
    }

}
