package com.example.bdandroid;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaRegistros extends AppCompatActivity {
    private Conexion c;
    private ListView lv2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_registros);
        c = new Conexion(this,"DBconsumidores",null,1);
        consultarConsumo();
    }




    public void consultarConsumo(){
            SQLiteDatabase op = c.getReadableDatabase();
            Cursor cursor = op.rawQuery("SELECT * FROM consumidores",null);
            lv2 = findViewById(R.id.lv2);
            lv2.setAdapter(null);
            ArrayList<String> r = new ArrayList<>();

            if(cursor.moveToFirst()) {
                do {
                    r.add(cursor.getString(0) + " - " + cursor.getString(1) + " - " + cursor.getString(2) + " - " + cursor.getString(3) + " - " + cursor.getString(4));
                }while(cursor.moveToNext());
                cursor.close();
            }

            if(r.isEmpty()){
                mostrarMensaje("¡NO hay resgistros!.");
            }

                ArrayAdapter<String> adapt = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, r);
                lv2.setAdapter(adapt);
                op.close();
    }

    public void regresar(View v){
        finish();
    }

    public void mostrarMensaje(String m){
        Toast msj = Toast.makeText(getApplicationContext(),m,Toast.LENGTH_LONG);
        msj.show();
    }

}
