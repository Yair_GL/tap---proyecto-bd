package com.example.bdandroid;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Registros extends AppCompatActivity {
    private Conexion c;
    private EditText n,m,userN;
    private int i=0;
    private int lecAc;
    private int lecAn;
    private int consumoAgua;
    private double importePagar;
    private int numeroUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registros);
        c = new Conexion(this,"DBconsumidores",null,1);

    }

    public void insertar(View v){
        n = findViewById(R.id.lecAnterior);
        m = findViewById(R.id.lecActual);
        userN = findViewById(R.id.nuser);
        if(!(n.getText().toString().trim().length()==0) && !(m.getText().toString().trim().length()==0) && !(userN.getText().toString().trim().length()==0)) {
            SQLiteDatabase op = c.getWritableDatabase();
            String anterior = n.getText().toString();
            String actual = m.getText().toString();

                lecAc = Integer.parseInt(m.getText().toString());
                lecAn = Integer.parseInt(n.getText().toString());
                numeroUsuario = Integer.parseInt(userN.getText().toString());
                if(lecAc>=lecAn) {
                    //        db.execSQL("CREATE TABLE consumidores("+"id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, nusuario INTEGER, actual INTEGER, anterior INTEGER, pago REAL)");
                    consumoAgua = lecAc - lecAn;

                    int costo_MT3=10;
                    int precioMantenimiento=5;

                    importePagar = consumoAgua*costo_MT3 + (consumoAgua*0.18) + precioMantenimiento;
                    String imP = String.format("%.2f",importePagar);
                    double pagarR = Double.parseDouble(imP);

                    op.execSQL("INSERT INTO consumidores(nusuario,anterior,actual,pago)  values ('"+numeroUsuario+"', '"+lecAn+"', '"+lecAc+"', '"+pagarR+"')");
                    op.close();
                    Toast.makeText(this,"Guardado correctamente, ususario: " + numeroUsuario,Toast.LENGTH_LONG).show();
                }else{
                    mostrarMensaje("¡El Valor Actual debe ser MAYOR o IGUAL al Valor Anterior!");

                }

            }else{
                //lanza mensaje con el toast :v
                mostrarMensaje("¡Los campos estan VACIOS!");
            }



    }

    public void mostrarMensaje(String m){
        Toast msj = Toast.makeText(getApplicationContext(),m,Toast.LENGTH_LONG);
        msj.show();
    }

    public void regresar(View v){
        finish();
    }
}
