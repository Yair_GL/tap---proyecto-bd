package com.example.bdandroid;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
private Conexion c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c = new Conexion(this,"DBconsumidores",null,1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);

    }

    public void insertarDB(View v){
        SQLiteDatabase op = c.getWritableDatabase();
        op.execSQL("INSERT INTO consumidores(nombre)  values ('Rogelio')");
        op.close();
        Toast.makeText(this,"Guardado correctamente.",Toast.LENGTH_LONG).show();
    }

    public void eliminarDB(View v){
        SQLiteDatabase op = c.getWritableDatabase();
        op.execSQL("DELETE FROM consumidores");
        op.execSQL("DELETE FROM sqlite_sequence WHERE name = 'consumidores'");
        op.close();
    }

    public void devolverDB(View v){
        SQLiteDatabase op = c.getReadableDatabase();
        String resultados="";
        Cursor cursor = op.rawQuery("SELECT * FROM consumidores",null);
        //cursor.moveToFirst()
        if(cursor.moveToFirst()){
            do{
                //  db.execSQL("CREATE TABLE consumidores("+"id INTEGER PRIMARY KEY AUTOINCREMENT, nusuario INTEGER, actual INTEGER, anterior INTEGER, pago REAL)");

                resultados += cursor.getString(0)+".- "+  "USUARIO:"+ cursor.getInt(1) + " - " +  cursor.getInt(2)  + " - "+  cursor.getInt(3)  + " - "+ cursor.getFloat(4)+ "\n";
                        Toast.makeText(this,resultados,Toast.LENGTH_LONG).show();

            }while (cursor.moveToNext());
            cursor.close();
        }
    }

    public void insertarRegistroA(View v){
        Intent intencion = new Intent(this,Registros.class);

        startActivity(intencion);
    }

    public void pagarConsumo(View v){
        Intent intencion2 = new Intent(this,PagarConsumo.class);
        startActivity(intencion2);
    }

    public void listarRegistrosA(View v){
        Intent intencion3 = new Intent(this,ListaRegistros.class);
        startActivity(intencion3);
    }

    public void listarConsumoU(View v){
        Intent intencion4 = new Intent(this,ListaRegistroUsuario.class);
        startActivity(intencion4);
    }


}
