package com.example.bdandroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Conexion extends SQLiteOpenHelper {


    public Conexion(Context context,String name,SQLiteDatabase.CursorFactory factory,int version) {
        super(context, name,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE consumidores("+"id INTEGER PRIMARY KEY AUTOINCREMENT, nusuario INTEGER, actual INTEGER, anterior INTEGER, pago REAL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
